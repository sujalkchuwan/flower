package routes

import (
	"flower/controller"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func InitializeRoutes() {
	router := mux.NewRouter()

	router.HandleFunc("/flower", controller.Addmeeting).Methods("POST")
	router.HandleFunc("/flower/{metid}", controller.GetMeet).Methods("GET")
	router.HandleFunc("/flower/{metid}", controller.UpdateMeet).Methods("PUT")
	router.HandleFunc("/flower/{metid}", controller.DeleteMeet).Methods("DELETE")
	// router.Handle("/flower", controller.GetAllmeeting)
	router.HandleFunc("/flower", controller.GetAllmeeting)

	router.HandleFunc("/signup", controller.Signup).Methods("POST")
	router.HandleFunc("/login", controller.Login).Methods("POST")
	router.HandleFunc("/logout", controller.Logout)
	// router.HandleFunc("/admin", controller.GetAdmin).Methods("GET")

	router.HandleFunc("/inventory", controller.Addflower).Methods("POST")
	router.HandleFunc("/inventory/{fid}", controller.GetFlow).Methods("GET")
	router.HandleFunc("/inventory/{fid}", controller.UpdateFlow).Methods("PUT")

	fhandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fhandler)

	log.Println("Application running on port 8081..")
	log.Fatal(http.ListenAndServe(":8081", router))

}
