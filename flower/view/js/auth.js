if (document.cookie === "") {
    alert("User not logged in!!")
    window.open("index.html", "_self")
} else {
    console.log("cookie set")
}


document.querySelector(".logout-logout").addEventListener("click", () => {
    logout();
});

function logout() {
    fetch('/logout', {
        method: "POST"
    })
        .then(response => {
            if (response.ok) {
                window.open("index.html", "_self");
            } else {
                throw new Error(response.statusText);
            }
        }).catch(e => {
            alert(e);
        });
}