CREATE DATABASE my_db;

CREATE TABLE meeting (
MeetingID int NOT NULL,
Location varchar(45) NOT NULL,
Date int not NULL,
Status varchar(45) NOT NULL,
PRIMARY KEY (MeetingID),
UNIQUE (Location)
)
