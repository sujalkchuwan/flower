package model

import "flower/dataStore/postgres"

type Flower struct {
	FlowerId   int64  `json:"flowerid"`
	FlowerName string `json:"fname"`
	Qty        int64  `json:"qty"`
	Totalbill  int32  `json:"total"`
}

const (
	queryUser       = "INSERT INTO flower(flowerid, flowername, qty, totalbill) VALUES ($1,$2,$3,$4)"
	querygetUser    = "SELECT flowerid, flowername, qty, totalbill FROM Flower WHERE flowerid=$1"
	queryUpdateflow = "UPDATE flowerid SET folwerid=$1, flowername=$2, qty=$3, totalbill=$4 WHERE flowerid = $5 RETURING flowerid"
)

func (f *Flower) Create() error {
	_, err := postgres.Db.Exec(queryUser, f.FlowerId, f.FlowerName, f.Qty, f.Totalbill)
	return err
}

func (f *Flower) Read() error {
	return postgres.Db.QueryRow(querygetUser, f.FlowerId).Scan(&f.FlowerId, &f.FlowerName, &f.Qty, &f.Totalbill)
}

func (f *Flower) UPDATE(oldFID int64) error {
	err := postgres.Db.QueryRow(queryUpdateflow, f.FlowerId, f.FlowerName, f.Qty, f.Totalbill).Scan(&f.FlowerId)
	return err
}
