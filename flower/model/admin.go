package model

import "flower/dataStore/postgres"

type Admin struct {
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
	Email     string `json:"email"`
	Password  string `json:"password"`
}

const queryInsertAdmin = "INSERT INTO admin(firstname,lastname,email,password) VALUES($1, $2, $3, $4);"

// const queryGetAdminDetails = "SELECT email, password ,firstname , lastname FROM admin where email=$3"

const queryGetAdmin = "SELECT email, password FROM admin WHERE email=$1 and password=$2;"

func (adm *Admin) Create() error {
	_, err := postgres.Db.Exec(queryInsertAdmin, adm.FirstName, adm.LastName, adm.Email, adm.Password)
	return err
}

func (adm *Admin) Get() error {
	return postgres.Db.QueryRow(queryGetAdmin, adm.Email,
		adm.Password).Scan(&adm.Email, &adm.Password)
}

// func (adm *Admin) Read() error {
// 	row := postgres.Db.QueryRow(queryGetAdminDetails, adm.Email)
// 	err := row.Scan(&adm.Email, &adm.FirstName, &adm.LastName, &adm.Password)
// 	return err
// }
