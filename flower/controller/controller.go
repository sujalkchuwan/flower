package controller

import (
	"database/sql"
	"encoding/json"
	"flower/model"
	"flower/utils/httpResp"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func Addmeeting(w http.ResponseWriter, r *http.Request) {

	var met model.Meeting

	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&met); err != nil {
		w.Write([]byte("Invalid json data"))
	}

	defer r.Body.Close()

	saveErr := met.Create()
	if saveErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}

	httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "appoiment added"})

}

func GetMeet(w http.ResponseWriter, r *http.Request) {
	metid := mux.Vars(r)["metid"]
	oldmeetingid, idErr := getUserId(metid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest,
			idErr.Error())
		return
	}
	m := model.Meeting{MeetingID: oldmeetingid}
	getErr := m.Read()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "appoiment not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, m)
}

func getUserId(userIdParam string) (int64, error) {
	userId, userErr := strconv.ParseInt(userIdParam, 10, 64)
	if userErr != nil {
		return 0, userErr
	}
	return userId, nil
}
func UpdateMeet(w http.ResponseWriter, r *http.Request) {
	old_metid := mux.Vars(r)["metid"]
	oldmeetingid1, idErr := getUserId(old_metid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest,
			idErr.Error())
		return
	}

	var meet model.Meeting

	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&meet); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()

	updateErr := meet.Update(oldmeetingid1)

	if updateErr != nil {
		switch updateErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "appoiment not found")
		default:
			httpResp.RespondWithError(w,
				http.StatusInternalServerError, updateErr.Error())
		}

	} else {
		httpResp.RespondWithJSON(w, http.StatusOK, meet)
	}

}

func DeleteMeet(w http.ResponseWriter, r *http.Request) {
	metid := mux.Vars(r)["metid"]
	oldmeetingid, idErr := getUserId(metid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest,
			idErr.Error())
		return
	}

	m := model.Meeting{MeetingID: oldmeetingid}
	if err := m.Delete(); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "deleted"})
}

func GetAllmeeting(w http.ResponseWriter, r *http.Request) {
	Meeting, getErr := model.GetAllmets()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, Meeting)
}
