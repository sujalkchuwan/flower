package controller

import (
	"encoding/json"
	"flower/model"
	"flower/utils/httpResp"
	"net/http"
	"time"
)

func Signup(w http.ResponseWriter, r *http.Request) {
	var admin model.Admin
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&admin); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}

	defer r.Body.Close()
	saveErr := admin.Create()
	if saveErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "admin added"})

}

func Login(w http.ResponseWriter, r *http.Request) {

	var admin model.Admin
	err := json.NewDecoder(r.Body).Decode(&admin)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	getErr := admin.Get()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusUnauthorized, getErr.Error())
		return
	}

	cookie := http.Cookie{
		Name:    "my-cookie",
		Value:   "cookie-value",
		Expires: time.Now().Add(30 * time.Minute),
		Secure:  true,
	}
	http.SetCookie(w, &cookie)
	httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"Status": "Login Success"})

}
func Logout(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{ // create and set the cookie in one line
		Name:    "my-cookie",
		Expires: time.Now(),
	})

	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "logout success"})
}

func VerifyCookie(w http.ResponseWriter, r *http.Request) bool {
	cookie, err := r.Cookie("my-cookie")
	if err != nil {
		if err == http.ErrNoCookie {
			httpResp.RespondWithError(w, http.StatusSeeOther, "Cookie not set")
			return false
		}
		httpResp.RespondWithError(w, http.StatusInternalServerError, "internal server error")
		return false
	}
	// validate cookie value
	if cookie.Value != "cookie-value" {
		httpResp.RespondWithError(w, http.StatusSeeOther, "Cookie value does not match")
		return false
	}

	return true
}

// func GetAdmin(w http.ResponseWriter, r *http.Request) {
// 	cookie, err := r.Cookie("my-cookie")
// 	if err != nil {
// 		if err == http.ErrNoCookie {
// 			httpResp.RespondWithError(w, http.StatusUnauthorized, err.Error())
// 		} else {
// 			httpResp.RespondWithError(w, http.StatusBadRequest, "error reading cookie: "+err.Error())
// 		}
// 		return
// 	}

// 	email := cookie.Value
// 	fmt.Println(email)

// 	adm := model.Admin{Email: email}

// 	getErr := adm.Read()
// 	if getErr != nil {
// 		switch getErr {
// 		case sql.ErrNoRows:
// 			httpResp.RespondWithError(w, http.StatusNotFound, "Admin not found")
// 		default:
// 			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
// 		}
// 		return
// 	}

// 	httpResp.RespondWithJSON(w, http.StatusOK, adm)
// }
