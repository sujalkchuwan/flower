package main

import "flower/routes"

func main() {
	routes.InitializeRoutes()
}
